import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {GameOutcomeModalData} from "../model/GameOutcomeModalData";
import {GameOutcome} from "../model/RockpaperscissorsResponseDto";

@Component({
    selector: 'app-game-outcome-modal',
    templateUrl: './game-outcome-modal.component.html',
    styleUrls: ['./game-outcome-modal.component.css']
})
export class GameOutcomeModalComponent {

    gameOutcomeEnum = GameOutcome;

    gameOutcome!: string;


    constructor(
        public dialogRef: MatDialogRef<GameOutcomeModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: GameOutcomeModalData
    ) {
        console.log(data);
    }

}
