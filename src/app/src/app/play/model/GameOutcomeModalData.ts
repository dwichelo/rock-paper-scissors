import {RockpaperscissorsResponseDto} from "./RockpaperscissorsResponseDto";

export interface GameOutcomeModalData {
    name: string;
    response: RockpaperscissorsResponseDto
}
