import {Hand} from "./Hand";

export interface Player {
    hand: Hand
}
