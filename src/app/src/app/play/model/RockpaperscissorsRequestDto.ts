import {Hand} from "./Hand";

export interface RockpaperscissorsRequestDto {
    hand: Hand
}
