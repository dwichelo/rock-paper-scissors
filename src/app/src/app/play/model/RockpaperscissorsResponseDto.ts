import {Player} from "./Player";

class RockpaperscissorsResponseDto {

    constructor(
        public challenger: Player,
        public challenged: Player,
        public outcome: GameOutcome
    ) {
    }

    static init(challenger: Player, challenged: Player, outcome: string) {
        return new RockpaperscissorsResponseDto(challenger, challenged, GameOutcome[outcome as GameOutcome]);
    }
}

enum GameOutcome {
    WIN = "WIN", LOSE = "LOSE", DRAW = "DRAW"
}

export {RockpaperscissorsResponseDto, GameOutcome};
