import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {RockpaperscissorsApiService} from "./service/rockpaperscissors-api.service";
import {RockpaperscissorsResponseDto} from "./model/RockpaperscissorsResponseDto";
import {MatDialog} from "@angular/material/dialog";
import {GameOutcomeModalComponent} from "./game-outcome-modal/game-outcome-modal.component";

@Component({
    selector: 'app-play',
    templateUrl: './play.component.html',
    styleUrls: ['./play.component.css']
})
export class PlayComponent implements OnInit {

    form!: FormGroup;

    isPlayButtonDisabled = true;


    constructor(
        private formBuilder: FormBuilder,
        private service: RockpaperscissorsApiService,
        private dialog: MatDialog
    ) {
    }

    ngOnInit(): void {
        this.setForm();
        this.onValuesChangeListener();
    }

    setForm() {
        this.form = this.formBuilder.group({
            name: [null],
            handChoice: [null]
        });
    }

    onValuesChangeListener() {
        this.form.valueChanges.subscribe(values =>
            this.isPlayButtonDisabled = values.name == null || values.handChoice == null
        )
    }

    play() {
        const request = {hand: this.form.get("handChoice")!.value}
        this.service.play(request).subscribe(response =>
            this.showOutcomeModal(response)
        );
    }

    showOutcomeModal(response: RockpaperscissorsResponseDto) {
        this.dialog.open(GameOutcomeModalComponent, {
            width: '30vw',
            data: {name: this.form.get("name")!.value, response},
        });
    }

}
