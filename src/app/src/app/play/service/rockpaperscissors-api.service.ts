import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {RockpaperscissorsRequestDto} from "../model/RockpaperscissorsRequestDto";
import {RockpaperscissorsResponseDto} from "../model/RockpaperscissorsResponseDto";
import {map, Observable} from "rxjs";
import {environment} from "../../../environments/environment";

@Injectable({
    providedIn: 'root'
})
export class RockpaperscissorsApiService {

    endpoint = `${environment.baseUrl}/rockpaperscissors`;

    constructor(
        private http: HttpClient
    ) {
    }

    play(request: RockpaperscissorsRequestDto): Observable<RockpaperscissorsResponseDto> {
        return this.http.post<RockpaperscissorsResponseDto>(this.endpoint, request)
            .pipe(
                map(response =>
                    RockpaperscissorsResponseDto.init(response.challenger, response.challenged, response.outcome)
                ));
    }

}
