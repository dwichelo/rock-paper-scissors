package com.wichelo.rockpaperscissors.controller

import com.wichelo.rockpaperscissors.controller.dto.RockpaperscissorsRequestDto
import com.wichelo.rockpaperscissors.controller.dto.RockpaperscissorsResponseDto
import com.wichelo.rockpaperscissors.controller.dto.toPlayer
import com.wichelo.rockpaperscissors.service.RockpaperscissorsService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/rockpaperscissors")
class RockpaperscissorsController(
    val service: RockpaperscissorsService
) {

    @PostMapping()
    @CrossOrigin("http://localhost:4200")
    fun play(
        @RequestBody request: RockpaperscissorsRequestDto
    ): ResponseEntity<RockpaperscissorsResponseDto> {
        val challenger = request.toPlayer()
        val response = service.play(challenger)
        return ResponseEntity(response, HttpStatus.OK)
    }

}