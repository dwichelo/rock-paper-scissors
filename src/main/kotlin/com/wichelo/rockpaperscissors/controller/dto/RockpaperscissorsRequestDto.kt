package com.wichelo.rockpaperscissors.controller.dto

import com.wichelo.rockpaperscissors.domain.Hand
import com.wichelo.rockpaperscissors.domain.Player

data class RockpaperscissorsRequestDto(
    val hand: String
)

fun RockpaperscissorsRequestDto.toPlayer() = Player(
    Hand.valueOf(hand)
)