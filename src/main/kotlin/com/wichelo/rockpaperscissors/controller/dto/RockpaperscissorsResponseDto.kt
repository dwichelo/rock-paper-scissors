package com.wichelo.rockpaperscissors.controller.dto

import com.wichelo.rockpaperscissors.domain.GameOutcome
import com.wichelo.rockpaperscissors.domain.Player as domainPlayer

class RockpaperscissorsResponseDto(
    val challenger: Player,
    val challenged: Player,
    val outcome: String
) {
    companion object {
        fun fromDomain(challenger: domainPlayer, challenged: domainPlayer, outcome: GameOutcome): RockpaperscissorsResponseDto {
            return RockpaperscissorsResponseDto(
                Player.fromDomainPlayer(challenger),
                Player.fromDomainPlayer(challenged),
                outcome.toString()
            )
        }
    }
}


class Player(
    val hand: String
) {

    companion object {
        fun fromDomainPlayer(player: domainPlayer): Player {
            return Player(player.hand.toString())
        }

    }
}

