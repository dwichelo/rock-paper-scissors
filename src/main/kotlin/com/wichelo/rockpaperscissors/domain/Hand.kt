package com.wichelo.rockpaperscissors.domain

enum class Hand(value: String) {
    ROCK("ROCK"), PAPER("PAPER"), SCISSORS("SCISSORS");

    fun isEqual(other: Hand): Boolean {
        return this == other
    }

    fun isGreaterThan(other: Hand): Boolean {
        return (this == ROCK && other == SCISSORS) ||
                (this == PAPER && other == ROCK) ||
                (this == SCISSORS && other == PAPER)
    }

}