package com.wichelo.rockpaperscissors.domain

import java.util.*

data class Player(
    val hand: Hand
) {

    companion object {
        fun create(): Player = Player(Hand.values()[Random().nextInt(Hand.values().size)])
    }

}

