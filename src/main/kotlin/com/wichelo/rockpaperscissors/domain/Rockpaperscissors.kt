package com.wichelo.rockpaperscissors.domain

data class Rockpaperscissors(
    val challenger: Player,
    val challenged: Player
) {

    val outcome: GameOutcome by lazy {
        if (challenger.hand.isEqual(challenged.hand)) {
            GameOutcome.DRAW
        } else if (challenger.hand.isGreaterThan(challenged.hand)) {
            GameOutcome.WIN
        } else {
            GameOutcome.LOSE
        }
    }

}

enum class GameOutcome {
    WIN, LOSE, DRAW
}