package com.wichelo.rockpaperscissors.service

import com.wichelo.rockpaperscissors.controller.dto.RockpaperscissorsResponseDto
import com.wichelo.rockpaperscissors.domain.Player
import com.wichelo.rockpaperscissors.domain.Rockpaperscissors
import org.springframework.stereotype.Service

@Service
class RockpaperscissorsService {

    fun play(challenger: Player): RockpaperscissorsResponseDto {
        val challenged = Player.create()
        Rockpaperscissors(challenger, challenged).let {
            return RockpaperscissorsResponseDto.fromDomain(it.challenger, it.challenged, it.outcome)
        }
    }

}