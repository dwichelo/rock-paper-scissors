package com.wichelo.rockpaperscissors.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.wichelo.rockpaperscissors.controller.dto.RockpaperscissorsRequestDto
import com.wichelo.rockpaperscissors.controller.dto.RockpaperscissorsResponseDto
import com.wichelo.rockpaperscissors.domain.Hand
import com.wichelo.rockpaperscissors.domain.Player
import com.wichelo.rockpaperscissors.service.RockpaperscissorsService
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import com.wichelo.rockpaperscissors.controller.dto.Player as dtoPlayer

@ExtendWith(SpringExtension::class)
@WebMvcTest(RockpaperscissorsController::class)
class RockpaperscissorsControllerTests {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockBean
    private lateinit var service: RockpaperscissorsService


    @Test
    fun `should return a winning outcome`() {
        val request = RockpaperscissorsRequestDto("SCISSORS")
        val expected = RockpaperscissorsResponseDto(dtoPlayer("SCISSORS"), dtoPlayer("PAPER"), "WIN")

        `when`(service.play(Player(Hand.SCISSORS))).thenReturn(expected)

        val content = toJSON(request)
        mockMvc.perform(post("/rockpaperscissors")
            .contentType(MediaType.APPLICATION_JSON)
            .content(content))
            .andExpect {
                status().isOk
                Assertions.assertEquals(it.response.contentAsString, toJSON(expected))
            }
    }

    private fun toJSON(dto: Any): String {
        try {
            return ObjectMapper().writeValueAsString(dto)
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }

}