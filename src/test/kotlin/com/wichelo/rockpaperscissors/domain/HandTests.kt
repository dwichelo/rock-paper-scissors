package com.wichelo.rockpaperscissors.domain

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class HandTests {

    @Test
    fun `hand should be equal`() {
        Assertions.assertTrue(Hand.ROCK.isEqual(Hand.ROCK))
        Assertions.assertTrue(Hand.PAPER.isEqual(Hand.PAPER))
        Assertions.assertTrue(Hand.SCISSORS.isEqual(Hand.SCISSORS))
    }

    @Test
    fun `hand should be greater than`() {
        Assertions.assertTrue(Hand.ROCK.isGreaterThan(Hand.SCISSORS))
        Assertions.assertTrue(Hand.PAPER.isGreaterThan(Hand.ROCK))
        Assertions.assertTrue(Hand.SCISSORS.isGreaterThan(Hand.PAPER))
    }

}