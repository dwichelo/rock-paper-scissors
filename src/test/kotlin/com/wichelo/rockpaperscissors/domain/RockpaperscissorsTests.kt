package com.wichelo.rockpaperscissors.domain

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class RockpaperscissorsTests {

    @Test
    fun `outcome should be a draw if both hands are identical`() {
        val rock = Rockpaperscissors(Player(Hand.ROCK), Player(Hand.ROCK))
        val paper = Rockpaperscissors(Player(Hand.PAPER), Player(Hand.PAPER))
        val scissors = Rockpaperscissors(Player(Hand.SCISSORS), Player(Hand.SCISSORS))

        Assertions.assertEquals(rock.outcome, GameOutcome.DRAW)
        Assertions.assertEquals(paper.outcome, GameOutcome.DRAW)
        Assertions.assertEquals(scissors.outcome, GameOutcome.DRAW)
    }

    @Test
    fun `outcome should be a  win if challenger's hand beats the challenged`() {
        val rock = Rockpaperscissors(Player(Hand.ROCK), Player(Hand.SCISSORS))
        val paper = Rockpaperscissors(Player(Hand.PAPER), Player(Hand.ROCK))
        val scissors = Rockpaperscissors(Player(Hand.SCISSORS), Player(Hand.PAPER))

        Assertions.assertEquals(rock.outcome, GameOutcome.WIN)
        Assertions.assertEquals(paper.outcome, GameOutcome.WIN)
        Assertions.assertEquals(scissors.outcome, GameOutcome.WIN)
    }

    @Test
    fun `outcome should be a loss if the challenged hand beats the challenger's hand`() {
        val rock = Rockpaperscissors(Player(Hand.ROCK), Player(Hand.PAPER))
        val paper = Rockpaperscissors(Player(Hand.PAPER), Player(Hand.SCISSORS))
        val scissors = Rockpaperscissors(Player(Hand.SCISSORS), Player(Hand.ROCK))

        Assertions.assertEquals(rock.outcome, GameOutcome.LOSE)
        Assertions.assertEquals(paper.outcome, GameOutcome.LOSE)
        Assertions.assertEquals(scissors.outcome, GameOutcome.LOSE)
    }

}